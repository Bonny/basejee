package com.soap.test;

import hello.service.com.HelloSvr_PortType;
import hello.service.com.HelloSvr_ServiceLocator;

public class HelloTest {

	public static void main(String[] args) {

		try {
			
			HelloSvr_ServiceLocator loc = new HelloSvr_ServiceLocator();
			loc.setHelloPortEndpointAddress("http://localhost:8080/SoapModule/HelloSvr/Hello?wsdl");

			HelloSvr_PortType p = loc.getHelloPort();

			System.out.println(p.hello("hello!!!"));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
