/**
 * HelloSvr_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hello.service.com;

public interface HelloSvr_Service extends javax.xml.rpc.Service {
    public java.lang.String getHelloPortAddress();

    public hello.service.com.HelloSvr_PortType getHelloPort() throws javax.xml.rpc.ServiceException;

    public hello.service.com.HelloSvr_PortType getHelloPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
