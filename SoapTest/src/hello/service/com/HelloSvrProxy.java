package hello.service.com;

public class HelloSvrProxy implements hello.service.com.HelloSvr_PortType {
  private String _endpoint = null;
  private hello.service.com.HelloSvr_PortType helloSvr_PortType = null;
  
  public HelloSvrProxy() {
    _initHelloSvrProxy();
  }
  
  public HelloSvrProxy(String endpoint) {
    _endpoint = endpoint;
    _initHelloSvrProxy();
  }
  
  private void _initHelloSvrProxy() {
    try {
      helloSvr_PortType = (new hello.service.com.HelloSvr_ServiceLocator()).getHelloPort();
      if (helloSvr_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)helloSvr_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)helloSvr_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (helloSvr_PortType != null)
      ((javax.xml.rpc.Stub)helloSvr_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public hello.service.com.HelloSvr_PortType getHelloSvr_PortType() {
    if (helloSvr_PortType == null)
      _initHelloSvrProxy();
    return helloSvr_PortType;
  }
  
  public java.lang.String hello(java.lang.String arg0) throws java.rmi.RemoteException{
    if (helloSvr_PortType == null)
      _initHelloSvrProxy();
    return helloSvr_PortType.hello(arg0);
  }
  
  
}