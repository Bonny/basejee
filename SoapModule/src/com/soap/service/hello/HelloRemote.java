package com.soap.service.hello;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "HelloSvr", targetNamespace = "com.service.hello")
public interface HelloRemote {

	@WebMethod
	public String hello(@WebParam String str);
}
