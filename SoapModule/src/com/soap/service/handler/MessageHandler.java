package com.soap.service.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

import com.commons.app.AppProperties.Keys;
import com.ejb.beans.IGlobalPropertiesLocal;

/**
 * intercetta il messaggio soap e lo stampa nel log
 * 
 * @author lbonaldo
 * @version 1.0
 *
 */
public class MessageHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Logger LOGGER = Logger.getLogger(MessageHandler.class);

	@EJB
	private IGlobalPropertiesLocal globalProperties;
	
	private boolean isDebug() {
		return Boolean.getBoolean(globalProperties.getProperty(Keys.SOAP_DEBUG_ENABLE));
	}

	public Set<QName> getHeaders() {
		return new HashSet<>();
	}

	public void close(MessageContext context) {

	}

	public boolean handleFault(SOAPMessageContext context) {
		return true;
	}

	public boolean handleMessage(SOAPMessageContext context) {

		Boolean out = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			context.getMessage().writeTo(baos);
		} catch (SOAPException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		}

		if (isDebug())
			if (!out)
				LOGGER.info("Inbound message: " + baos.toString());
			else
				LOGGER.info("Outbound message: " + baos.toString());
		else
			LOGGER.info("Debug is disabled");

		return true;
	}

}
