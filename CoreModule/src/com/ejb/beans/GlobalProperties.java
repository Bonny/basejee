package com.ejb.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.commons.app.AppProperties.Keys;
import com.commons.utility.Utils;
import com.ejb.jms.SenderBean;

/**
 * Singleton per inizializzazione delle proprietà di sistema sistema
 * 
 * @author lbonaldo
 * @since 1.0.0
 * @version 1.0.0
 * 
 */
@Singleton
@LocalBean
@Startup
@Local(IGlobalPropertiesLocal.class)
public class GlobalProperties implements IGlobalPropertiesLocal {

	private static final Logger logger = Logger.getLogger(GlobalProperties.class);

	/**
	 * 
	 */
	private final String ENV_PROP_FILE_PATH = "confpath";

	/**
	 * 
	 */
	private final String PROP_FILE_NAME = "app.properties";

	/**
	 * 
	 */
	private final Properties properties = new Properties();
	
	@EJB
	private SenderBean sender;

	/**
	 * Load the properties
	 */
	@PostConstruct
	void run() {

		FileInputStream fis = null;

		try {

			String initConfigPath = System.getProperty(ENV_PROP_FILE_PATH);

			if (Utils.isNullOrEmpty(initConfigPath))
				throw new Exception("var " + ENV_PROP_FILE_PATH + " not found");

			File configFile = new File(initConfigPath, PROP_FILE_NAME);

			if (!configFile.exists())
				throw new Exception("file " + PROP_FILE_NAME + " not exists");

			fis = new FileInputStream(configFile);
			// Loading the properties
			properties.load(fis);

			final String filePropS = getProperty(Keys.LOG_PROPERTIES_FILE_PATH);

			if (Utils.isNullOrEmpty(filePropS))
				throw new Exception("property " + Keys.LOG_PROPERTIES_FILE_PATH + " not found");

			final File fileProp = new File(filePropS);
			if (!fileProp.exists()) {
				logger.error("Logger properties file (" + filePropS + ") not found!");
			} else {
				PropertyConfigurator.configure(filePropS);
			}

			logger.info("Configuration file: " + configFile.getPath());

			print();
			
			sender.test();

		} catch (Exception e) {
			logger.error("Error loading properties", e);
			throw new EJBException("App initialization error", e);
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	/**
	 * Printing the properties
	 */
	void print() {
		logger.info("### Properties ###");
		Enumeration<Object> keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			logger.info(String.format("### %s -> [%s] ###", key, properties.getProperty(key)));
		}
		logger.info("###     End    ###");
	}

	@Override
	public String getProperty(Keys key) {
		return properties.getProperty(key.getValue());
	}
}
