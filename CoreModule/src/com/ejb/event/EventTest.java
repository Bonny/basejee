package com.ejb.event;

import javax.annotation.PostConstruct;
import javax.ejb.PostActivate;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.log4j.Logger;

@Singleton
@Startup
public class EventTest {

	private static final Logger LOGGER = Logger.getLogger(EventTest.class);
	
	//@Inject
	//private EventSource es;

	public EventTest() {
		//for (int i = 0; i < 10; i++) es.fireEvent("msg_" + i);
	}
	
	@PostActivate
	public void postActivate(){
		LOGGER.debug("PostActivate");
	}
	
	@PostConstruct
	public void postConstruct(){
		LOGGER.debug("PostConstruct");
	}
}
