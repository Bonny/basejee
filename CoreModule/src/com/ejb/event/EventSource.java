package com.ejb.event;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

@Named
@RequestScoped
public class EventSource {

	private static final Logger LOGGER = Logger.getLogger(EventSource.class);

	@Inject
	private Event<String> simpleMessageEvent;

	public void fireEvent(String msg) {
		LOGGER.info("fireEvent: " + msg);
		simpleMessageEvent.fire(msg);
	}
}
