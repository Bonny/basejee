package com.ejb.event;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import org.apache.log4j.Logger;

@Named
@ApplicationScoped
public class EventObserver {

	private static final Logger LOGGER = Logger.getLogger(EventObserver.class);

	public void observeEvent(@Observes String message) {
		LOGGER.info("observeEvent: " + message);
	}
}
