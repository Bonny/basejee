package com.ejb.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.commons.app.Constants;

@MessageDriven(name = "ObjectReceiver", activationConfig = {
		//@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1"),
		//@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		//@ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = Constants.TEST_QUEUE),
		//@ActivationConfigProperty(propertyName = "DLQMaxResent", propertyValue = "1"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'object_example'") 
		}
)
public class ObjectReceiver implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(ObjectReceiver.class);

	public void onMessage(Message message) {

		try {
			// if(!message.getJMSRedelivered()){

			if (message instanceof ObjectMessage) {
				ObjectMessage objectMessage = (ObjectMessage) message;
				LOGGER.info(String.format("RECEIVED[%s] --> %s", message.getStringProperty("messageType"),
						(String) objectMessage.getObject()));
			} else {
				LOGGER.warn("message is not instance of ObjectMessage");
			}
			
			// }
		} catch (JMSException e) {
			LOGGER.error(e);
		}
	}

}
