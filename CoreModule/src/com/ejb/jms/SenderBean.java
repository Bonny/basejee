package com.ejb.jms;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.commons.app.Constants;

@Stateless
@LocalBean
public class SenderBean {

	private static final Logger LOGGER = Logger.getLogger(SenderBean.class);

	@Resource(mappedName = "java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName = Constants.TEST_QUEUE)
	private Destination receptor;

	public void test() {
		try {
		
			Connection connect = connectionFactory.createConnection();
			Session session = connect.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(receptor);

			final ObjectMessage objMsg = session.createObjectMessage();
			objMsg.setStringProperty("messageType", "object_example");
			objMsg.setObject("HELLO I AM OBJ MESSAGE !!!");
			
			LOGGER.info("SEND[object_example] --> " + objMsg.getObject());
			producer.send(objMsg);

			
			final TextMessage txtMsg = session.createTextMessage();
			txtMsg.setStringProperty("messageType", "text_example");
			txtMsg.setText("HELLO I AM TEXT MESSAGE !!!");
			
			LOGGER.info("SEND[text_example] --> " + txtMsg.getText());
			producer.send(txtMsg);
						
			producer.close();
			
		} catch (Throwable e) {
			LOGGER.error(e);
		}
	}


}
