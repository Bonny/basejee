package com.ejb.jms;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

@Singleton
@Startup
public class SenderBean {

	private static final Logger LOGGER = Logger.getLogger(SenderBean.class);

	@Resource(mappedName = "java:/JmsXA")
	public QueueConnectionFactory factory;

	@Resource(mappedName = "queue/testQueue")
	private Queue destination;

	// @PostConstruct
	void test() {

		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;

		try {

			connection = factory.createConnection();

			// creating a session in auto ack without transaction

			session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);

			producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);

			// Creating the message with persistent Delivery mode

			TextMessage tm = session.createTextMessage();
			tm.setStringProperty("messageType", "text_example");

			producer.send(tm);

			ObjectMessage om = session.createObjectMessage("HELLO TEST !!!");
			om.setStringProperty("messageType", "object_example");

			producer.send(om);

			producer.close();

		}

		catch (Throwable e) {
			LOGGER.error(e);
		} finally {

			try {

				if (session != null)

					session.close();

				if (connection != null)

					connection.close();

			} catch (JMSException exc) {
				LOGGER.error(exc);
			}

		}
	}

}
