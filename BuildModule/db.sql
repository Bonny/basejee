CREATE TABLE users (
    id        varchar(32)  NOT NULL,
    pwd       varchar(32)  NOT NULL,
    dins      timestamp(3) NOT NULL DEFAULT NOW()     
);

ALTER TABLE users ADD CONSTRAINT PK_users PRIMARY KEY (ID);