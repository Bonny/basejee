package com.web.restful.init;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.log4j.Logger;

import com.restful.service.impl.Auth;
import com.restful.service.impl.Hello;

@ApplicationPath("/rest")
public class Restful extends Application {

	private static final Logger LOGGER = Logger.getLogger(Restful.class);
	private Set<Object> singletons = new HashSet<>();

	public Restful() {
		LOGGER.info("###### Start Restful ######");
		singletons.add(new Hello());
		singletons.add(new Auth());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
