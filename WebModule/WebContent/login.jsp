<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="header.jsp"%>

<script src="js/directives/loading.js" type="text/javascript"></script>
<script src="js/directives/translate.js" type="text/javascript"></script>

<div class="container" ng-controller="LoginController" style="padding-left: 50px;padding-right: 50px;" role="main">

  <form class="jumbotron animate-view" ng-submit="login()" role="form" style="background-color: #FFF;">

    <h2>
      <i class="fa fa-sign-in fa-fw" aria-hidden="true"></i>
      <span translate>login.title</span>
    </h2>

    <p style="font-size: 16px;">
      <span translate>login.disclaimer</span>
    </p>

    <div class="form-group">
      <label for="username">
        <span translate>login.userid</span>
      </label> 
      <input type="text" id="username" class="form-control" ng-model="username" autofocus>
    </div>

    <div class="form-group">
      <label for="password">
        Password
      </label> 
      <input type="password" id="password" class="form-control" ng-model="password">
    </div>

    <div class="row" style="margin-top: 30px;">

      <div class="col-sm-3 col-md-3">
        <button type="submit" class="form-control btn btn-primary">
          <span translate>login.btn</span>
        </button>
      </div>

      <div class="col-sm-1 col-md-1" ng-show="loading">
        <loading size="2"></loading>
      </div>

      <div class="col-sm-8 col-md-8" style="padding-top: 6px; ">
        <span id="login-msg" class="" style="color: red"></span>
      </div>

    </div>
  </form>

</div>


<script type="text/javascript">

  /* global app */

  app.controller('LoginController', ['$scope', '$http', '$location',
      function ($scope, $http, $location) {

          $location.path('/');

          $scope.username = "";
          $scope.password = "";

          $scope.loading = false;

          $scope.login = function () {

              if ($scope.username && $scope.password) {

                  $scope.loading = true;

                  $http({
                      method: "GET",
                      url: "rest/auth/login",
                      params: {
                          id: $scope.username,
                          pwd: $scope.password,
                          _dc: new Date().getTime()
                      }
                  }).then(loginSuccessCallback, loginErrorCallback);

              } else {
                  document.getElementById("login-msg").innerHTML = "<span translate>login.error</span>";
              }
          };

          var loginSuccessCallback = function (resp) {
              $scope.loading = false;
              window.location.reload(true);
          };

          var loginErrorCallback = function (result) {
              $scope.loading = false;
              document.getElementById("login-msg").innerHTML = result;
          };
      }]);
</script>



<%@ include file="footer.jsp"%>