/* global app */

var lang = {};
lang["login.title"] = "Accedi";
lang["login.disclaimer"] = "Inserisci negli appositi spazi il tuo nome utente e la password";
lang["login.userid"] = "Userid";
lang["login.btn"] = "Login";
lang["login.error"] = "Login non valida";
lang["msgBox.alert.default.msg"] = "Errore interno del server";

app.directive('translate', ["$log", function ($log) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                    $log.info(element);             
            }
        };
    }
]);


