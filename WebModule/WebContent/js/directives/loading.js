/* global app, angular */

app.directive('loading', [function () {
      return {
         restrict: 'E',
         scope: {
            size: '=?'
         },
         template: "<div style=\"color: #555;width:100%;text-align:center;padding: 0px;margin-top: 4px;\">"
                 + " <i class=\"fa fa-spinner fa-pulse fa-fw fa-{{faX}}\"></i>"
                 + " <span class=\"sr-only\" translate>loading.default.msg</span>"
                 + "</div>",
         controller: ['$scope', function ($scope) {

               // TODO implementare switch per size

               $scope.faX = angular.isDefined($scope.size) ? '2x' : '3x';
            }]
      };
   }
]);