<%@page import="java.io.File"%>
<%@page import="com.commons.app.AppProperties"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%!final String appTitle = String.format("%s v.%s", AppProperties.NAME, AppProperties.VER);%>
<%!
public String getStripts(final String dir){
   
   StringBuilder sb = new StringBuilder();
   
   File[] files = new File(getServletContext().getRealPath(dir)).listFiles(); 
   
   if (files != null) {
      for (File f : files) {
        if (f.exists() && f.isFile()) {
            sb.append(String.format("<script src=\"%s/%s?v=%s\" type=\"text/javascript\"></script>\n", dir, f.getName(), AppProperties.VER));
        }
      }
   }
 
   return sb.toString();
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="app">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><%=appTitle%></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    
      <!-- LIB STYLE -->

      <link href="lib/bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

      <!-- CUSTOM STYLE -->

      <link href="css/spaces.css" rel="stylesheet" type="text/css"/>
      <link href="css/style.css" rel="stylesheet" type="text/css"/>
      <link href="css/animate.css" rel="stylesheet" type="text/css"/>

      <!-- CUSTOM EXTRA STYLE -->

      <!-- LIB JS -->

      <!--[if lt IE 9]>
      <script src="lib/html5shiv.min.js"></script>
      <script src="lib/respond.min.js"></script>
      <![endif]-->
      
      <script src="lib/moment.min.js" type="text/javascript"></script>
      <script src="lib/angular/angular.min.js" type="text/javascript"></script>
      <script src="lib/angular/angular-animate.min.js" type="text/javascript"></script>
      <script src="lib/angular/angular-ui-router.min.js" type="text/javascript"></script>
      <script src="lib/angular/angular-messages.js" type="text/javascript"></script>
      
      <script src="lib/jquery/jquery.min.js" type="text/javascript"></script>
      <script src="lib/jquery/jquery.fileDownload.js" type="text/javascript"></script>
      <script src="lib/bootstrap-3.3.6/js/bootstrap.min.js" type="text/javascript"></script>

      <script src="lib/angular/ng-file-upload-shim.min.js" type="text/javascript"></script>
      <script src="lib/angular/ng-file-upload.min.js" type="text/javascript"></script>

      <script src="lib/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
      <script src="lib/tinymce/js/tinymce/angularui.tinymce.min.js" type="text/javascript"></script>

    <script type="text/javascript">
      
      
         var app = angular.module('app', ['ui.router', 'ui.tinymce', 'ngAnimate', 'ngFileUpload', 'ngMessages']);

         app.constant('config', {
            'appName': '<%=AppProperties.NAME%>',
            'appVersion': '<%=AppProperties.VER%>',
            'pageSize': 10
         });


    </script>
    
  </head>
  <body>