package com.restful.beans;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {

	// "expanded": false,
	private boolean expanded = false;
	// "children": [],
	private List<TreeNode> children;
	// "checked": false,
	private boolean checked = false;
	// "text": "user.name" + i,
	private String text;
	// "iconCls": "user",
	private String iconCls;
	// "leaf": true,
	private boolean leaf = false;
	// "idx": i,
	private Long idx;
	// "qtip": null,
	private String qtip = "";
	// "admin": false
	private boolean admin = false;

	public TreeNode() {
		this.children = new ArrayList<>();
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Long getIdx() {
		return idx;
	}

	public void setIdx(Long idx) {
		this.idx = idx;
	}

	public String getQtip() {
		return qtip;
	}

	public void setQtip(String qtip) {
		this.qtip = qtip;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public String toString() {
		return "TreeNode [children=" + children + ", checked=" + checked + ", text=" + text + ", leaf=" + leaf
				+ ", idx=" + idx + "]";
	}



}
