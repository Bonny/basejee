package com.restful.beans;

public class Result {

	private Object data;
	private Long total = 0L;

	public Result(Object data) {
		super();
		this.data = data;
	}

	public Result(Object data, long total) {
		super();
		this.data = data;
		this.total = total;
	}

	public Result() {
		super();
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

}
