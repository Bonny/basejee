package com.restful.beans;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

import com.commons.utility.Utils;

public class PageParam extends BaseParam {

	@QueryParam("sort_field")
	private String sortField;

	@QueryParam("sort_dir")
	@DefaultValue("ASC")
	private String sortDir;

	@QueryParam("filter_rel")
	@DefaultValue("AND")
	private String filterRel;

	@QueryParam("filter_data")
	@DefaultValue("[]")
	private String filterData;

	@QueryParam("range_limit")
	@DefaultValue("20")
	private int limit;

	@QueryParam("range_offset")
	@DefaultValue("0")
	private int offset;

	public static enum SortDir {
		ASC, DESC
	};

	public static enum FilterRel {
		AND, OR
	};

	public FilterRel getFilterRel() {
		return (Utils.isNotNullOrEmpty(filterRel) && FilterRel.OR.name().equalsIgnoreCase(filterRel)) ? FilterRel.OR
				: FilterRel.AND;
	}

	public SortDir getSortDir() {
		return (Utils.isNotNullOrEmpty(sortDir) && SortDir.DESC.name().equalsIgnoreCase(sortDir)) ? SortDir.DESC
				: SortDir.ASC;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		return super.toString() + ", PageParam [sortField=" + sortField + ", sortDir=" + sortDir + ", filterRel="
				+ filterRel + ", filterData=" + filterData + ", limit=" + limit + ", offset=" + offset + "]";
	}

}
