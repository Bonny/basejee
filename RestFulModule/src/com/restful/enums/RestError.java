package com.restful.enums;

import org.json.JSONException;
import org.json.JSONObject;

public enum RestError {

	/**
	 * 500 - Generic error
	 */
	EG001(500, "Generic error"),
	/**
	 * 500 - Database error
	 */
	EG002(500, "Database error"),
	/**
	 * 401 - Authentication failed
	 */
	EA001(401, "Authentication failed"),
	/**
	 * 401 - Access not permited
	 */
	EA002(401, "Access not permited");

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private String msg;
	private int status;

	private RestError(int status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public String toJSON() {
		JSONObject o = new JSONObject();
		try {
			o.put("http-status", getStatus());
			o.put("error-msg", getMsg());
			o.put("error-code", name());
		} catch (JSONException e) {
		}
		return o.toString();
	}

}
