package com.restful.exception;

import com.restful.enums.RestError;

public class RestException extends Exception {

	private static final long serialVersionUID = 1L;
	private RestError restError = RestError.EG001;

	public RestException(Throwable cause) {
		super(cause);
	}

	public RestException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestException(RestError restError, String message, Throwable cause) {
		super(message, cause);
		setRestError(restError);
	}

	public RestException(String message) {
		super(message);
	}

	public RestException() {
		super();
	}

	public RestException(RestError restError, Throwable cause) {
		super(cause);
		setRestError(restError);
	}

	public RestException(RestError restError) {
		super();
	}

	public RestError getRestError() {
		return restError;
	}

	private void setRestError(RestError restError) {
		this.restError = restError;
		setRestError(restError);
	}

}
