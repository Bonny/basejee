package com.restful.exception;

import javax.ejb.EJBException;

public class SessionException extends EJBException {


	private static final long serialVersionUID = 1L;

	public SessionException() {
		// TODO Auto-generated constructor stub
	}

	public SessionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SessionException(Exception ex) {
		super(ex);
		// TODO Auto-generated constructor stub
	}

	public SessionException(String message, Exception ex) {
		super(message, ex);
		// TODO Auto-generated constructor stub
	}

}
