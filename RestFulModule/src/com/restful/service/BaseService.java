package com.restful.service;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
/*
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TransactionRequiredException;
*/
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.restful.beans.BaseParam;
import com.restful.beans.Result;
import com.restful.enums.RestError;
import com.restful.exception.RestException;


public abstract class BaseService {
	
	@Context
	protected HttpServletRequest servletRequest;
	
	public String getLogPrefix(){
		return String.format("[sessUUID=%s]", servletRequest.getSession().getId());
	}
    
	
	protected static final Logger LOGGER = Logger.getLogger(BaseService.class);
	private long startRequestMS = 0L;
/*
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private EntityManager createEntityManager() throws RestException {
		try {
			final Map<String, String> props = new TreeMap<>();
			// props.put("hibernate.default_schema", "");
			// props.put("hibernate.ejb.entitymanager_factory_name", "");

			EntityManagerFactory emf = Persistence.createEntityManagerFactory(null, props);
			EntityManager target = emf.createEntityManager();

			return (EntityManager) Proxy.newProxyInstance(this.getClass().getClassLoader(),
					new Class<?>[] { EntityManager.class }, (proxy, method, args) -> {
						try {
							target.joinTransaction();
						} catch (TransactionRequiredException e) {
							LOGGER.warn(e.getMessage());
						}
						return method.invoke(target, args);
					});

		} catch (Exception ex) {
			throw new RestException(RestError.EG002, "CREATE ENTITY MANAGER EXCEPTION", ex);
		} catch (Error er) {
			throw new RestException(RestError.EG002, "CREATE ENTITY MANAGER ERROR", er);
		}
	}
*/
	protected void start() {
		this.startRequestMS = System.currentTimeMillis();
		LOGGER.info("Start service: " + getClass().getSimpleName());
	}

	protected void start(BaseParam bp) {
		start();
		LOGGER.info(bp);
	}
	
	protected void end() {
		long took = System.currentTimeMillis() - this.startRequestMS;
		LOGGER.info("End service : " + getClass().getSimpleName() + " in " + took + "(ms)");
	}

	protected Response ok() {
		return Response.ok().build();
	}

	protected Response ok(Object obj) {
		return Response.ok(obj).build();
	}

	protected Response ok(JSONObject obj) {
		return obj != null ? Response.ok(obj.toString()).build() : ok();
	}

	protected Response ok(JSONArray obj) {
		return obj != null ? Response.ok(obj.toString()).build() : ok();
	}

	protected Response ok(Result obj) {
		return obj != null ? ok(new JSONObject(obj)) : ok();
	}

	protected Response exception(Exception ex) {

		if (ex != null)
			LOGGER.error("SERVICE EXCEPTION", ex);

		if (ex != null && ex instanceof RestException) {
			final RestError re = ((RestException) ex).getRestError();
			return Response.status(re.getStatus()).entity(re.toJSON()).build();
		} else
			return Response.status(RestError.EG001.getStatus()).entity(RestError.EG001.toJSON()).build();
	}

	protected Response error(Error er) {
		if (er != null)
			LOGGER.error("SERVICE ERROR", er);
		return Response.status(RestError.EG001.getStatus()).entity(RestError.EG001.toJSON()).build();
	}
}
