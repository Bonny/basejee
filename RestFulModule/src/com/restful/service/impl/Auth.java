package com.restful.service.impl;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.da.beans.impl.UsersDA;
import com.restful.beans.Result;
import com.restful.service.BaseService;

@Path("/auth")
public class Auth extends BaseService {
	
	@EJB(lookup="java:module/UsersDA!com.da.beans.impl.UsersDA")
	private UsersDA usersDA;

	@GET
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@QueryParam(value = "id") String id, @QueryParam(value = "pwd") String pwd) {
		try {
			return ok(new Result(usersDA.auth(id, pwd, getLogPrefix())));
		} catch (Exception ex) {
			return exception(ex);
		} catch (Error e) {
			return error(e);
		} finally {
			end();
		}
	}
}
