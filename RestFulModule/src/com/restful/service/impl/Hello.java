package com.restful.service.impl;

import java.util.Date;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.restful.beans.BaseParam;
import com.restful.beans.Result;
import com.restful.service.BaseService;

@Path("/hello")
public class Hello extends BaseService {

	@GET
	@Path("world")
	@Produces(MediaType.APPLICATION_JSON)
	public Response world(@BeanParam BaseParam base) {
		try {
			start(base);
			return ok(new Result(new Date().getTime()));
		} catch (Exception ex) {
			return exception(ex);
		} catch (Error e) {
			return error(e);
		} finally {
			end();
		}
	}
}
