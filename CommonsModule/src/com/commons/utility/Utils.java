package com.commons.utility;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public final class Utils {

	private Utils() {
	}
	
	public static boolean isNullOrEmpty(String value) {

		if (value == null || value.trim().length() == 0)
			return true;
		else
			return false;
	}

	public static boolean isNotNullOrEmpty(String value) {
		return !isNullOrEmpty(value);
	}

	public static boolean isNullOrEmpty(Number value) {
		return isNullOrEmpty(value, true);
	}

	public static boolean isNullOrEmpty(Number value, boolean nullIfZero) {

		if (value == null)
			return true;
		else {
			if (nullIfZero && value.intValue() == 0)
				return true;
			else
				return false;
		}
	}

	public static boolean isNullOrEmpty(Object[] value) {

		if (value == null || value.length == 0)
			return true;
		else
			return false;
	}

	public static boolean isNullOrEmpty(byte[] value) {

		if (value == null || value.length == 0)
			return true;
		else
			return false;
	}

	public static boolean isNullOrEmpty(Collection<?> value) {

		if (value == null || value.size() == 0)
			return true;
		else
			return false;
	}

	public static boolean isNullOrEmpty(Map<?, ?> value) {

		if (value == null || value.size() == 0)
			return true;
		else
			return false;
	}

	public static boolean isNullOrEmpty(List<?> value) {

		if (value == null || value.size() == 0)
			return true;
		else
			return false;
	}

	public static String formatDirectory(String dirValue) {
		if (isNullOrEmpty(dirValue))
			return null;
		else {
			if (dirValue.endsWith(System.getProperty("file.separator")))
				return dirValue;
			else
				return dirValue + System.getProperty("file.separator");
		}
	}


}
