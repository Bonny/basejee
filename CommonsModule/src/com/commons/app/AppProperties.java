package com.commons.app;

public final class AppProperties {

	private AppProperties() {
	}

	public static final String NAME = "APP NAME";
	public static final String VER = "1.0.0";

	public static enum Keys {

		/**
		 * 
		 */
		LOG_PROPERTIES_FILE_PATH("log.properties.file.path"),
		/**
		 * 
		 */
		SOAP_DEBUG_ENABLE("soap.debub.enable")

		;
		private String value;

		private Keys(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

}
