package com.da.beans.impl;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.commons.utility.Utils;
import com.da.beans.IUserDALocal;
import com.da.entity.User;
import com.da.enums.AuthResult;

/**
 * Session Bean implementation class UsersDA
 */
@Stateless
@LocalBean
@Local(IUserDALocal.class)
public class UsersDA implements IUserDALocal {

	private static final Logger logger = Logger.getLogger(UsersDA.class);

	@PersistenceContext
	private EntityManager em;

	public AuthResult auth(String id, String pwd, String logPrefix) {

		AuthResult ar = AuthResult.FAIL;

		try {

			logger.info(String.format("%s Start method auth id=%s, pwd=%s", logPrefix, id,
					Utils.isNotNullOrEmpty(pwd) ? "not-null" : "null"));

			User u = (User) em.createQuery("select u from User u where id=:id and pwd=:pwd").setParameter("id", id)
					.setParameter("pwd", pwd).getSingleResult();

			logger.debug(String.format("%s Found %s", logPrefix, u));

		} catch (NoResultException ne) {
			logger.warn(String.format("%s User not found", logPrefix));
		} catch (Exception ex) {
			logger.error(String.format("%s Error during method auth", logPrefix), ex);
		} finally {
			logger.info(String.format("%s End method", logPrefix));
		}

		return ar;
	}

}
