package com.da.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column
	@Id
	private String id;

	@Column
	private String pwd;

	@Column(updatable = false, nullable = false)
	private Date dIns;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Date getdIns() {
		return dIns;
	}

	public void setdIns(Date dIns) {
		this.dIns = dIns;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", dIns=" + dIns + "]";
	}

	@PrePersist
	void prePersist() {
		if (dIns == null)
			dIns = new Date();
	}
}
